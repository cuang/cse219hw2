package mv.workspace;

import com.sun.java.accessibility.util.AWTEventMonitor;
import djf.components.AppWorkspaceComponent;
import static djf.modules.AppGUIModule.DISABLED;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import java.awt.event.MouseEvent;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import properties_manager.PropertiesManager;
import mv.MapViewerApp;
import static mv.MapViewerPropertyType.MV_ITEMS_PANE;
import static mv.MapViewerPropertyType.MV_ITEM_BUTTONS_PANE;
import static mv.MapViewerPropertyType.MV_LABEL;
import static mv.MapViewerPropertyType.MV_MAP_PANE;
import static mv.MapViewerPropertyType.MV_MOVE_DOWN_BUTTON;
import static mv.MapViewerPropertyType.MV_MOVE_LEFT_BUTTON;
import static mv.MapViewerPropertyType.MV_MOVE_RIGHT_BUTTON;
import static mv.MapViewerPropertyType.MV_MOVE_UP_BUTTON;
import static mv.MapViewerPropertyType.MV_PANE;
import static mv.MapViewerPropertyType.MV_RESET_LOCATION_BUTTON;
import static mv.MapViewerPropertyType.MV_RESET_ZOOM_BUTTON;
import static mv.MapViewerPropertyType.MV_ZOOM_IN_BUTTON;
import static mv.MapViewerPropertyType.MV_ZOOM_OUT_BUTTON;
import mv.data.MapViewerData;
import mv.workspace.controllers.ItemsController;
import static mv.workspace.style.MapViewerStyle.CLASS_MV_BIG_HEADER;
import static mv.workspace.style.MapViewerStyle.CLASS_MV_BOX;
import static mv.workspace.style.MapViewerStyle.CLASS_MV_BUTTON;
import static mv.workspace.style.MapViewerStyle.CLASS_MV_MAP_OCEAN;
import static mv.workspace.style.MapViewerStyle.CLASS_MV_MAP_BOX;
import static mv.workspace.style.MapViewerStyle.CHANGE_REGION_COLOR;
import java.awt.Point;
import javafx.scene.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javafx.scene.control.ScrollPane;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Scale;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.json.JsonObject;
import mv.data.MapViewerData;

/**
 *
 * @author McKillaGorilla
 */
public class MapViewerWorkspace extends AppWorkspaceComponent {

    private static double pressX, pressY, mouseX, mouseY;
    private static double realScaleX, realScaleY;
    private static double mouseOnPolygonID, numOfPolygon;
    private boolean moveOnPolygon;
    private Scale newScale;
    double maxX, maxY = 0;
    double minX, minY = 9999;
    
    public MapViewerWorkspace(MapViewerApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();
    }
        
    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder workspaceBuilder = app.getGUIModule().getNodesBuilder();
        
	// THIS IS WHERE WE'LL DRAW THE MAP
        Pane mapPane = new Pane();
        //Pane tool = new Pane();
        
        VBox toolPane = workspaceBuilder.buildVBox(MV_PANE, null, null, CLASS_MV_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        toolPane.setMaxWidth(400);
        Label toolLabel = workspaceBuilder.buildLabel(MV_LABEL, toolPane, null, CLASS_MV_BIG_HEADER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        VBox firstRow              = workspaceBuilder.buildVBox(MV_ITEMS_PANE,                 toolPane,       null,   CLASS_MV_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        HBox controlMapPane        = workspaceBuilder.buildHBox(MV_ITEM_BUTTONS_PANE,          firstRow,          null,   null, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button resetNaviButton     = workspaceBuilder.buildIconButton(MV_RESET_ZOOM_BUTTON,   controlMapPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        Button fitToPolyButton       = workspaceBuilder.buildIconButton(MV_RESET_LOCATION_BUTTON,     controlMapPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        Button zoomInButton     = workspaceBuilder.buildIconButton(MV_ZOOM_IN_BUTTON,       controlMapPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        Button zoomOutButton   = workspaceBuilder.buildIconButton(MV_ZOOM_OUT_BUTTON,     controlMapPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        
        VBox secondRow              = workspaceBuilder.buildVBox(MV_ITEMS_PANE,                 toolPane,       null,   CLASS_MV_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        HBox moveButtonsPane        = workspaceBuilder.buildHBox(MV_ITEM_BUTTONS_PANE,          secondRow,          null,   null, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button moveMapLeftButton       = workspaceBuilder.buildIconButton(MV_MOVE_LEFT_BUTTON,     moveButtonsPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        Button moveMapRightButton     = workspaceBuilder.buildIconButton(MV_MOVE_RIGHT_BUTTON,   moveButtonsPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        Button moveMapUpButton     = workspaceBuilder.buildIconButton(MV_MOVE_UP_BUTTON,       moveButtonsPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        Button moveMapDownButton   = workspaceBuilder.buildIconButton(MV_MOVE_DOWN_BUTTON,     moveButtonsPane,    null,   CLASS_MV_BUTTON, HAS_KEY_HANDLER,  FOCUS_TRAVERSABLE,  ENABLED);
        

        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        toolPane.getChildren().add(browser);        
        
        // AND THIS WILL BE USED TO CLIP THE MAP SO WE CAN ZOOM
        BorderPane outerMapPane = new BorderPane();
        outerMapPane.setCenter(mapPane);

        
        Rectangle clippingRectangle = new Rectangle();
        outerMapPane.setClip(clippingRectangle);        
        Pane clippedPane = new Pane();
        outerMapPane.setCenter(clippedPane);
        clippedPane.getChildren().add(mapPane);
        Rectangle ocean = new Rectangle();
        mapPane.getChildren().add(ocean);
        ocean.getStyleClass().add(CLASS_MV_MAP_OCEAN);
        app.getGUIModule().addGUINode(MV_MAP_PANE, mapPane);
        mapPane.minWidthProperty().bind(outerMapPane.widthProperty());
        mapPane.maxWidthProperty().bind(outerMapPane.widthProperty());
        mapPane.minHeightProperty().bind(outerMapPane.heightProperty());
        mapPane.maxHeightProperty().bind(outerMapPane.heightProperty());
        outerMapPane.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            clippingRectangle.setWidth(newValue.getWidth());
            clippingRectangle.setHeight(newValue.getHeight());
            ocean.setWidth(newValue.getHeight()*2);
            ocean.setHeight(newValue.getHeight());
        });
        

        // AND PUT EVERYTHING IN THE WORKSPACE
	workspace = new BorderPane();
//        workspace.getChildren().(new MyMouseListener());
	((BorderPane)workspace).setCenter(outerMapPane);
        ((BorderPane)workspace).setLeft(toolPane);
        
        resetNaviButton.setOnAction(e->{
            mapPane.setTranslateX(0);
            mapPane.setTranslateY(0);
            mapPane.setScaleX(1);
            mapPane.setScaleY(1);
            newScale.setX(1);
            newScale.setY(1);
            
            updateWebView(mapPane, ocean, webEngine);
        });
        fitToPolyButton.setOnAction(e->{


            for(int i = 1; i < mapPane.getChildren().size(); i ++){
                Polygon p = (Polygon)(mapPane.getChildren().get(i));
                for(int j = 0; j < p.getPoints().size(); j++){
                    double d = p.getPoints().get(j);
                    if(j % 2 == 0){//x cordinates
                        if(d < minX)
                            minX = d;
                        if(d > maxX)
                            maxX = d;
                    }
                    else if (j % 2 == 1){// y cordinates
                        if(d < minY)
                            minY = d;
                        if(d > maxY)
                            maxY = d;
                    }
                }
            }
            
            double zoomX = mapPane.getWidth() / (maxX - minX) * 0.9;
            double zoomY = mapPane.getHeight() / (maxY - minY) * 0.9;
            if(zoomX < zoomY){
                mapPane.setScaleX(zoomX);
                mapPane.setScaleY(zoomX);
            }
            else{
                mapPane.setScaleX(zoomY);
                mapPane.setScaleY(zoomY);
            }
            
            double marginX = mapPane.getWidth() / 10;
            double marginY = mapPane.getHeight() / 10;
            
            
            mapPane.setTranslateX(marginX);
            mapPane.setTranslateY(marginY);
            
            updateWebView(mapPane, ocean, webEngine);
        });
        zoomInButton.setOnAction(e->{
            double xScale = mapPane.getScaleX();
            double yScale = mapPane.getScaleY();
            double zoomInX = xScale * 2;
            double zoomInY = yScale * 2;
            mapPane.setScaleX(zoomInX);
            mapPane.setScaleY(zoomInY);
            updateWebView(mapPane, ocean, webEngine);
        });
        zoomOutButton.setOnAction(e->{
            double xScale = mapPane.getScaleX();
            double yScale = mapPane.getScaleY();
            if(xScale < 2 || yScale < 2){
                
            }
            else{
                double zoomInX = xScale / 2;
                double zoomInY = yScale / 2;
                mapPane.setScaleX(zoomInX);
                mapPane.setScaleY(zoomInY);
            }
            updateWebView(mapPane, ocean, webEngine);
        });
        moveMapRightButton.setOnAction(e-> {
            double x = mapPane.getTranslateX();
            double y = mapPane.getTranslateY();
            double xMoved =(x+100);
            mapPane.setTranslateX(xMoved);
            updateWebView(mapPane, ocean, webEngine);
        });
        moveMapLeftButton .setOnAction(e->{
            double x = mapPane.getTranslateX();
            double y = mapPane.getTranslateY();
            double xMoved =(x-100);
            mapPane.setTranslateX(xMoved);
            updateWebView(mapPane, ocean, webEngine);
        });      
        moveMapUpButton.setOnAction((ActionEvent e)->{
            double x = mapPane.getTranslateX();
            double y = mapPane.getTranslateY();
            double yMoved =(y-100);
            mapPane.setTranslateY(yMoved);
            updateWebView(mapPane, ocean, webEngine);
        });
        moveMapDownButton.setOnAction((ActionEvent e)->{
            double x = mapPane.getTranslateX();
            double y = mapPane.getTranslateY();
            double yMoved =(y+100);
            mapPane.setTranslateY(yMoved);
            updateWebView(mapPane, ocean, webEngine);
        });
        
        mapPane.setOnMouseMoved(e->{
            mouseX = e.getX();
            mouseY = e.getY();
            for(int i = 1; i < mapPane.getChildren().size(); i++){
                if(mapPane.getChildren().get(i).contains(mouseX, mouseY)){
                    mapPane.getChildren().get(i).setStyle("-fx-fill: linear-gradient(RED, PINK);");
                    Polygon p = (Polygon)(mapPane.getChildren().get(i));
                    numOfPolygon = p.getPoints().size()/2;
                }
                else{
                    mapPane.getChildren().get(i).setStyle("-fx-fill: linear-gradient(FORESTGREEN, GREENYELLOW);");
                }
            }
            updateWebView(mapPane, ocean, webEngine);
        });
        
        mapPane.setOnMouseDragged(e->{
            double mapX = mapPane.getTranslateX();
            double mapY = mapPane.getTranslateY();
            double scaleX = mapPane.getScaleX();
            double scaleY = mapPane.getScaleY();
            double x = e.getX();
            double y = e.getY();
            
            double dragX = (x - pressX)*scaleX;
            double dragY = (y - pressY)*scaleY;
            
            mapPane.setTranslateX(mapX + dragX);
            mapPane.setTranslateY(mapY + dragY);
            updateWebView(mapPane, ocean, webEngine);
        });
        mapPane.setOnMousePressed(e->{
            pressX = e.getX();
            pressY = e.getY();
            updateWebView(mapPane, ocean, webEngine);
        });
        mapPane.setOnMouseReleased(e->{
            double releaseX = e.getX();
            double releaseY = e.getY();
            updateWebView(mapPane, ocean, webEngine);
        });
        mapPane.setOnScroll(e->{           
            double zoom = 2;
            double deltaY = e.getDeltaY();

            if(deltaY < 0) {
                zoom = 2.5 - zoom;
            }

            newScale = new Scale();
            newScale.setPivotX(e.getX());
            newScale.setPivotY(e.getY());
            newScale.setX(mapPane.getScaleX() * zoom);            
            newScale.setY(mapPane.getScaleY() * zoom);
            
            mapPane.getTransforms().add(newScale);
            e.consume();
            updateWebView(mapPane, ocean, webEngine);
        });
        
        

    }
   
    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
//        System.out.println("WORKSPACE REPONSE TO " + ke.getCharacter());
    }
    
    
    public void updateWebView(Pane mapPane, Rectangle ocean, WebEngine webEngine){
        String HTML_STRING =
                "<table>"
                    +"<tbody>"
                        +"<tr>"
                            +"<td><b>Scale:</b></td>"
                            +"<td>"+mapPane.getScaleX()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>Viewport Width:</b></td>"
                            +"<td>"+mapPane.getWidth()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>Viewport Height:</b></td>"
                            +"<td>"+mapPane.getHeight()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>World Width:</b></td>"
                            +"<td>"+ ocean.getWidth()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>World Height:</b></td>"
                            +"<td>"+ocean.getHeight()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>World Mouse X:</b></td>"
                            +"<td>"+mouseX+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>World Mouse Y:</b></td>"
                            +"<td>"+mouseY+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>Viewport Mouse Percent X:</b></td>"
                            +"<td>"+ Double.parseDouble(String.format("%.1f",(mouseX / mapPane.getWidth()) * 100)) +"%</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>Viewport Mouse Percent Y:</b></td>"
                            +"<td>"+ Double.parseDouble(String.format("%.1f",(mouseY / mapPane.getHeight()) * 100)) +"%</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>World ViewportX:</b></td>"
                            +"<td>"+mapPane.getLayoutX()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b>World ViewportY:</b></td>"
                            +"<td>"+mapPane.getLayoutY()+"</td>"
                        +"</tr>"
                        +"<tr>"
                            +"<td><b># of Polygon Points:</b></td>"
                            +"<td>"+ numOfPolygon +"</td>"
                        +"</tr>"
                    +"</tbody>"
                +"</table>"
                ;
        webEngine.loadContent(HTML_STRING);
        
    }
}