package mv.workspace.style;

/**
 * This class lists all CSS style types for this application. These
 * are used by JavaFX to apply style properties to controls like
 * buttons, labels, and panes.

 * @author Richard McKenna
 * @version 1.0
 */
public class MapViewerStyle {
    public static final String EMPTY_TEXT = "";
    public static final int BUTTON_TAG_WIDTH = 75;

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS M3Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    // NOTE THAT FOUR CLASS STYLES ALREADY EXIST:
    // top_toolbar, toolbar, toolbar_text_button, toolbar_icon_button

    public static final String CLASS_MV_MAP_CLIP_PANE = "mv_map_clip_pane";
    public static final String CLASS_MV_MAP_OCEAN = "mv_map_ocean";
    public static final String CLASS_MV_MAP_BOX = "mv_map_box";
    public static final String CLASS_MV_BIG_HEADER = "mv_big_header";
    
    public static final String CLASS_MV_PANE = "mv_pane";
    public static final String CLASS_MV_BOX = "mv_box";
    public static final String CLASS_MV_BUTTON = "mv_button";
    public static final String CHANGE_REGION_COLOR = "color_change_region";
}