package mv.data;

import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import mv.MapViewerApp;
import static mv.MapViewerPropertyType.MV_MAP_PANE;
import java.awt.event.*; 
import java.awt.event.MouseEvent;
/**
 *
 * @author McKillaGorilla
 */
public class MapViewerData implements AppDataComponent {
    // THE APP ITSELF
    MapViewerApp app;

    // THE PANE WHERE WE'RE PUTTING ALL THE POLYGONS
    Pane map;

    // THE POLYGONS
    int subregionId;
    double translateX, translateY, scaleX, scaleY;
    
    HashMap<Integer, ObservableList<Polygon>> subregions;
    
    // LINE THICKNESS AT SCALE 1.0
    final double DEFAULT_LINE_THICKNESS = 1.0;

    /**
     * Constructor can only be called after the workspace
     * has been initialized because it retrieves the map pane.
     */
    public MapViewerData(MapViewerApp initApp) {
        app = initApp;
        subregions = new HashMap();
        map = (Pane)app.getGUIModule().getGUINode(MV_MAP_PANE);
    }    
  
    public HashMap getSubregionList(){
        return subregions;
    }
    
    public ObservableList<Polygon> getSubregion(int id) {
        return subregions.get(id);
    }
    public double getTransLateX(){
        return translateX;
    }
    public double getTransLateY(){
        return translateY;
    }
    public double getScaleX(){
        return scaleX;
    }
    public double getScaleY(){
        return scaleY;
    }
    public double getNumOfSubRegion(){
        return subregionId;
    }
    
    @Override
    public void reset() {
        // CLEAR THE DATA
        subregions.clear();
        subregionId = 0;
        
        // AND THE POLYGONS THEMSELVES
        Rectangle ocean = (Rectangle)map.getChildren().get(0);
        map.getChildren().clear();
        map.getChildren().add(ocean);
    }

    /**
     * For adding polygons to the map.
    */
    public void addSubregion(ArrayList<ArrayList<Double>> rawPolygons) {
        ObservableList<Polygon> subregionPolygons = FXCollections.observableArrayList();
        for (int i = 0; i < rawPolygons.size(); i++) {
            ArrayList<Double> rawPolygonPoints = rawPolygons.get(i);
            ArrayList numOfPolygon = new ArrayList();
            Polygon polygonToAdd = new Polygon();
            ObservableList<Double> transformedPolygonPoints = polygonToAdd.getPoints();
            for (int j = 0; j < rawPolygonPoints.size(); j+=2) {
                double longX = rawPolygonPoints.get(j);
                double latY = rawPolygonPoints.get(j+1);
                double x = longToX(longX);
                double y = latToY(latY);
                transformedPolygonPoints.addAll(x, y);
            }
            subregionPolygons.add(polygonToAdd);
            Stop[] stops = new Stop[] { new Stop(0, Color.FORESTGREEN), new Stop(1, Color.GREENYELLOW)}; 
            LinearGradient lg1 = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
            polygonToAdd.setFill(lg1);
            
            numOfPolygon.add(polygonToAdd.getPoints().size());
            polygonToAdd.setStroke(Color.BLACK);
            polygonToAdd.setStrokeWidth(DEFAULT_LINE_THICKNESS);
            polygonToAdd.setUserData(subregionId);
            map.getChildren().add(polygonToAdd);
        }
        subregions.put(subregionId, subregionPolygons);
        subregionId++;   
    }
    
    public void setLocateSize(double initTranslateX, double initTranslateY, double initScaleX, double initScaleY){
        
        translateX = initTranslateX;
        translateY = initTranslateY;
        scaleX = initScaleX;
        scaleY = initScaleY;
        
        map.setScaleX(scaleX);
        map.setScaleY(scaleY);
        map.relocate(translateX, translateY);
    }

    
    /**
     * This calculates and returns the x pixel value that corresponds to the
     * xCoord longitude argument.
     */
    private double longToX(double longCoord) {
        double paneHeight = map.getHeight();
        double unitDegree = paneHeight/180;
        double paneWidth = map.getWidth();
        double newLongCoord = (longCoord + 180) * unitDegree;
        return newLongCoord;
    }
    
    
    /**
     * This calculates and returns the y pixel value that corresponds to the
     * yCoord latitude argument.
     */
    private double latToY(double latCoord) {
        // DEFAULT WILL SCALE TO THE HEIGHT OF THE MAP PANE
        double paneHeight = map.getHeight();
        
        // WE ONLY WANT POSITIVE COORDINATES, SO SHIFT BY 90
        double unitDegree = paneHeight/180;
        double newLatCoord = (latCoord + 90) * unitDegree;
        return paneHeight - newLatCoord;
    }
    
}