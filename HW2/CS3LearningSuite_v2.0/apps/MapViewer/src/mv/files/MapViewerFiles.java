package mv.files;

import static djf.AppPropertyType.APP_EXPORT_PAGE;
import static djf.AppPropertyType.APP_PATH_EXPORT;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.swing.text.html.HTML;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import mv.MapViewerPropertyType;
import static mv.MapViewerPropertyType.MV_EXPORT_TEMPLATE_FILE_NAME;
import mv.data.MapViewerData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class MapViewerFiles implements AppFileComponent {
    static final String JSON_NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String JSON_SUBREGIONS = "SUBREGIONS";
    static final String JSON_SUBREGION_INDEX = "SUBREGION_INDEX";
    static final String JSON_NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGON = "SUBREGION_POLYGON";
    static final String JSON_POLYGON_POINTS = "VERTICES";
    static final String JSON_POLYGON_POINT_X = "X";
    static final String JSON_POLYGON_POINT_Y = "Y";
    static final String JSON_TRANSLATE_X = "TRANSLATEX";
    static final String JSON_TRANSLATE_Y = "TRANSLATEY";
    static final String JSON_SCALEX = "SCALEX";
    static final String JSON_SCALEY = "SCALEY";
    
    /**
     * This method is for saving user work.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	MapViewerData mapViewerData = (MapViewerData)data;
        
	// NOW BUILD THE JSON ARRAY FOR THE LIST
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject mapViewerDataJSO = Json.createObjectBuilder()
                .add(JSON_TRANSLATE_X, mapViewerData.getTransLateX())
                .add(JSON_POLYGON_POINT_Y, mapViewerData.getTransLateY())
                .add(JSON_SCALEX, mapViewerData.getScaleX())
                .add(JSON_SCALEY, mapViewerData.getScaleY())
                .add(JSON_NUMBER_OF_SUBREGIONS, mapViewerData.getNumOfSubRegion())
		.build();

        JsonArrayBuilder subRegionsArray = Json.createArrayBuilder();
        
        //subRegionObject is under subRegionsArray
//        JsonObjectBuilder subRegionsObject = Json.ObjectBuilder()
        for(int i = 0; i < mapViewerData.getSubregionList().size(); i++){
            ObservableList<Polygon> ob = mapViewerData.getSubregion(i);
            JsonArrayBuilder jb = Json.createArrayBuilder();
            for(int j = 0; j < ob.size(); j++){
                JsonArrayBuilder jb2 = Json.createArrayBuilder();
                for(int k = 0; k < ob.size(); k++){
//                    ObservableList<Polygon> ob2 = mapViewerData;
                }
            }
        }
        
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(mapViewerDataJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(mapViewerDataJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT WE ARE USING THE SIZE OF THE MAP
        MapViewerData mapData = (MapViewerData)data;
        mapData.reset();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
        // THIS IS THE TOTAL NUMBER OF SUBREGIONS, EACH WITH
        // SOME NUMBER OF POLYGONS
        int numSubregions = getDataAsInt(json, JSON_NUMBER_OF_SUBREGIONS);
        JsonArray jsonSubregionsArray = json.getJsonArray(JSON_SUBREGIONS);

        // GO THROUGH ALL THE SUBREGIONS
        for (int subregionIndex = 0; subregionIndex < numSubregions; subregionIndex++) {
            // MAKE A POLYGON LIST FOR THIS SUBREGION
            JsonObject jsonSubregion = jsonSubregionsArray.getJsonObject(subregionIndex);
            int numSubregionPolygons = getDataAsInt(jsonSubregion, JSON_NUMBER_OF_SUBREGION_POLYGONS);
            ArrayList<ArrayList<Double>> subregionPolygonPoints = new ArrayList();
            // GO THROUGH ALL OF THIS SUBREGION'S POLYGONS
            for (int polygonIndex = 0; polygonIndex < numSubregionPolygons; polygonIndex++) {
                // GET EACH POLYGON (IN LONG/LAT GEOGRAPHIC COORDINATES)
                JsonArray jsonPolygon = jsonSubregion.getJsonArray(JSON_SUBREGION_POLYGONS);
                JsonArray pointsArray = jsonPolygon.getJsonArray(polygonIndex);
                ArrayList<Double> polygonPointsList = new ArrayList();
                for (int pointIndex = 0; pointIndex < pointsArray.size(); pointIndex++) {
                    JsonObject point = pointsArray.getJsonObject(pointIndex);
                    double pointX = point.getJsonNumber(JSON_POLYGON_POINT_X).doubleValue();
                    double pointY = point.getJsonNumber(JSON_POLYGON_POINT_Y).doubleValue();
                    polygonPointsList.add(pointX);
                    polygonPointsList.add(pointY);
                }
                subregionPolygonPoints.add(polygonPointsList);
            }
            mapData.addSubregion(subregionPolygonPoints);
            double scaleX = getDataAsDouble(json, JSON_SCALEX);
            double scaleY = getDataAsDouble(json, JSON_SCALEY);
            double translateX = getDataAsDouble(json, JSON_TRANSLATE_X);
            double translateY = getDataAsDouble(json, JSON_TRANSLATE_Y);
            mapData.setLocateSize(translateX, translateY, scaleX, scaleY);
        }
    }
    
    
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method would be used to export data to another format,
     * which we're not doing in this assignment.
     */
    @Override
    public void exportData(AppDataComponent data, String savedFileName) throws IOException {
        String toDoListName = savedFileName.substring(0, savedFileName.indexOf("."));
        String fileToExport = toDoListName + ".html";
        try {
            // GET THE ACTUAL DATA
            MapViewerData mvData = (MapViewerData)data;
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String exportDirPath = props.getProperty(APP_PATH_EXPORT) + toDoListName + "/";
            File exportDir = new File(exportDirPath);
            if (!exportDir.exists()) {
                exportDir.mkdir();
            }

            // NOW LOAD THE TEMPLATE DOCUMENT
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            String htmlTemplatePath = props.getPropertiesDataPath() + props.getProperty(MV_EXPORT_TEMPLATE_FILE_NAME);
            File file = new File(htmlTemplatePath);
            System.out.println(file.getPath() + " exists? " + file.exists());
            URL templateURL = file.toURI().toURL();
            Document exportDoc = docBuilder.parse(templateURL.getPath());
            
            // CORRECT THE APP EXPORT PAGE
            props.addProperty(APP_EXPORT_PAGE, exportDirPath + fileToExport);

            // EXPORT THE WEB PAGE
            saveDocument(exportDoc, props.getProperty(APP_EXPORT_PAGE));
        }
        catch(SAXException | ParserConfigurationException
                | TransformerException exc) {
            throw new IOException("Error loading " + fileToExport);
        }
    }
    
        private void saveDocument(Document doc, String outputFilePath)
            throws TransformerException, TransformerConfigurationException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Result result = new StreamResult(new File(outputFilePath));
        Source source = new DOMSource(doc);
        transformer.transform(source, result);
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }
}